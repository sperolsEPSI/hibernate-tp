package fr.epsi.dao;

import fr.epsi.model.Analyse;

import java.util.List;

public class AnalyseDao extends AbstractDao<Analyse> {
    public List<Analyse> getAll() {
        return this.execute(s -> s.createQuery("from Analyse").list());
    }
}
