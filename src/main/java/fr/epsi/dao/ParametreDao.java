package fr.epsi.dao;

import fr.epsi.model.Parametre;
import fr.epsi.utils.MapUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParametreDao extends AbstractDao<Parametre> {
    public List<Parametre> getAll() {
        return this.execute(s -> s.createQuery("from Parametre").list());
    }

    public Map<Parametre, Integer> mostUsed() {
        Map<Parametre, Integer> result = new HashMap<>();

        List<Parametre> parametres = getAll();

        for (Parametre parametre : parametres) {
            result.put(parametre, parametre.getAnalyses().size());
        }

        return MapUtil.sortByValue(result);
    }

    public int customSave(Parametre object) {
        return this.execute((s) -> (int) s.save(object));
    }

    public Parametre customGet(int code) {
        return this.execute((s) -> (Parametre) session.get(getGenericType(), code));
    }
}
