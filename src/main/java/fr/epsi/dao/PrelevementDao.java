package fr.epsi.dao;

import fr.epsi.model.Prelevement;
import fr.epsi.model.Qualitometre;

import java.util.List;

public class PrelevementDao extends AbstractDao<Prelevement> {
    public List<Prelevement> getAll() {
        return this.execute(s -> s.createQuery("from Prelevement").list());
    }
}
