package fr.epsi.dao;

import fr.epsi.model.Qualitometre;

import java.util.List;

public class QualitometreDao extends AbstractDao<Qualitometre> {

    public List<Qualitometre> getAll() {
        return this.execute(s -> s.createQuery("from Qualitometre ").list());
    }

    public String customSave(Qualitometre object) {
        return this.execute((s) -> (String) s.save(object));
    }

    public Qualitometre customGet(String code) {
        return this.execute((s) -> (Qualitometre) session.get(getGenericType(), code));
    }
}
