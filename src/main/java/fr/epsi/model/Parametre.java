package fr.epsi.model;

import fr.epsi.enums.StatusEnum;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "parametre",
        uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Parametre {

    @Id
    private int code;
    private String nom;
    private Date dateCreation;
    private String auteur;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private StatusEnum statut;

    @OneToMany
    private List<Analyse> analyses;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public StatusEnum getStatut() {
        return statut;
    }

    public void setStatut(StatusEnum statut) {
        this.statut = statut;
    }

    public List<Analyse> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(List<Analyse> analyses) {
        this.analyses = analyses;
    }
}
