package fr.epsi.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "prelevement")
public class Prelevement implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateHeure;

    private String commentaire;

    @OneToMany(mappedBy = "prelevement", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Analyse> analyses;

    @ManyToOne
    private Qualitometre qualitometre;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(Date dateHeure) {
        this.dateHeure = dateHeure;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Qualitometre getQualitometre() {
        return qualitometre;
    }

    public void setQualitometre(Qualitometre qualitometre) {
        this.qualitometre = qualitometre;
    }

    public List<Analyse> getAnalyses() {
        return analyses;
    }

    public void setAnalyses(List<Analyse> analyses) {
        this.analyses = analyses;
    }
}
