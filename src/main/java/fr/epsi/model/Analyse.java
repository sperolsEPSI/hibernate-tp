package fr.epsi.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "analyse")
public class Analyse {

    @Id
    private long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateHeure;

    private float resultat;
    private int code;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private Prelevement prelevement;

    @ManyToOne
    private Parametre parametre;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(Date dateHeure) {
        this.dateHeure = dateHeure;
    }

    public float getResultat() {
        return resultat;
    }

    public void setResultat(float resultat) {
        this.resultat = resultat;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Prelevement getPrelevement() {
        return prelevement;
    }

    public void setPrelevement(Prelevement prelevement) {
        this.prelevement = prelevement;
    }

    public Parametre getParametre() {
        return parametre;
    }

    public void setParametre(Parametre parametre) {
        this.parametre = parametre;
    }
}
