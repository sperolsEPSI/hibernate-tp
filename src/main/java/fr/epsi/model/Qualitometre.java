package fr.epsi.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "qualitometre",
        uniqueConstraints = {@UniqueConstraint(columnNames = "code")})
public class Qualitometre implements Serializable {

    @Id
    private String code;
    private double longitude;
    private double latitude;
    private Date dateCreation;
    private String commentaire;

    @OneToMany(mappedBy = "qualitometre", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Prelevement> prelevements;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public List<Prelevement> getPrelevements() {
        return prelevements;
    }

    public void setPrelevements(List<Prelevement> prelevements) {
        this.prelevements = prelevements;
    }
}
