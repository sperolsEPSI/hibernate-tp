package fr.epsi.dao;

import fr.epsi.model.Analyse;
import fr.epsi.model.Parametre;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;

public class ParametreDaoTest {

    @Test
    public void insertParametre() {
        Parametre parametre = new Parametre();
        parametre.setCode(45);

        //long insertId = new AnalyseDao().save(analyse);

        LinkedList<Analyse> allAnalyse = new LinkedList<>(new AnalyseDao().getAll());

        //Assert.assertEquals(insertId, allAnalyse.getLast().getId());
    }

    @Test
    public void mostUsedParameter() {
        Parametre p1 = new Parametre();
        p1.setCode(1000);
        Parametre p2 = new Parametre();
        p2.setCode(1001);
        Parametre p3 = new Parametre();
        p3.setCode(1002);

        Analyse a1 = new Analyse();
        a1.setId(100);
        a1.setResultat(1.5f);
        Analyse a2 = new Analyse();
        a2.setId(101);
        a2.setResultat(2.0f);

        p1.setAnalyses(Collections.singletonList(a1));
        p2.setAnalyses(Collections.singletonList(a2));
        p3.setAnalyses(Collections.emptyList());

        a1.setParametre(p1);
        a2.setParametre(p2);

        new AnalyseDao().save(a1);
        new AnalyseDao().save(a2);

        Assert.assertEquals(Collections.emptyList(), new ParametreDao().mostUsed());
    }
}
