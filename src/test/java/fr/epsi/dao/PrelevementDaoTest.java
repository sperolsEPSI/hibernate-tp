package fr.epsi.dao;

import fr.epsi.model.Analyse;
import fr.epsi.model.Prelevement;
import fr.epsi.model.Qualitometre;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.Arrays;
import java.util.List;

public class PrelevementDaoTest {

    @Test
    public void insertPrelevement() {
        Qualitometre qualitometre = new Qualitometre();
        qualitometre.setCode("08035X0399");
        qualitometre.setLongitude(11.1342453647);
        qualitometre.setLatitude(11.1342453647);
        qualitometre.setDateCreation(new Date());
        qualitometre.setCommentaire("Ca marche !");

        Prelevement p1 = new Prelevement();
        p1.setDateHeure(new Date());
        Prelevement p2 = new Prelevement();
        p2.setDateHeure(new Date());
        Prelevement p3 = new Prelevement();
        p3.setDateHeure(new Date());

        List<Prelevement> prelevements = Arrays.asList(p1, p2, p3);
        prelevements.forEach(t -> t.setQualitometre(qualitometre));

        qualitometre.setPrelevements(prelevements);
        String qualitometreId = new QualitometreDao().customSave(qualitometre);

        Qualitometre newQualitometre = new QualitometreDao().customGet(qualitometreId);

        Assert.assertEquals(prelevements.size(), newQualitometre.getPrelevements().size());
    }

    @Test
    public void deletePrelevement() {
        Prelevement prelevement = new Prelevement();
        prelevement.setDateHeure(new Date());
        prelevement.setCommentaire("OUUUPPPSS");

        int nbAnalyses = new AnalyseDao().getAll().size();

        Analyse a1 = new Analyse();
        a1.setId(43);
        a1.setResultat(43.3f);
        Analyse a2 = new Analyse();
        a2.setId(44);
        a2.setResultat(46.3f);
        Analyse a3 = new Analyse();
        a3.setId(41);
        a3.setResultat(48.3f);

        List<Analyse> analyses = Arrays.asList(a1, a2, a3);
        prelevement.setAnalyses(analyses);
        analyses.forEach(t -> t.setPrelevement(prelevement));

        long prelevementId = new PrelevementDao().save(prelevement);

        Assert.assertEquals(analyses.size(), new PrelevementDao().get(prelevementId).getAnalyses().size());

        new PrelevementDao().delete(prelevement);

        Assert.assertEquals(nbAnalyses, new AnalyseDao().getAll().size());
    }

}
