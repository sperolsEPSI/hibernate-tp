package fr.epsi.dao;

import fr.epsi.model.Qualitometre;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedList;

public class QualitometreDaoTest {

    @Test
    public void insertUser() {
        Qualitometre qualitometre = new Qualitometre();
        qualitometre.setCode("08035X0398");
        qualitometre.setLongitude(11.1342453647);
        qualitometre.setLatitude(11.1342453647);
        qualitometre.setDateCreation(new Date());
        qualitometre.setCommentaire("Ca marche !");

        String code = new QualitometreDao().customSave(qualitometre);

        LinkedList<Qualitometre> allQualitometre = new LinkedList<>(new QualitometreDao().getAll());

        Assert.assertEquals(code, allQualitometre.getLast().getCode());
    }
}
