package fr.epsi.dao;

import fr.epsi.model.Analyse;
import fr.epsi.model.Qualitometre;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.LinkedList;

public class AnalyseDaoTest {

    @Test
    public void insertAnalyse() {
        Analyse analyse = new Analyse();
        analyse.setCode(4562);
        analyse.setDateHeure(new Date());
        analyse.setId(3);
        analyse.setResultat(34.5f);

        long insertId = new AnalyseDao().save(analyse);

        LinkedList<Analyse> allAnalyse = new LinkedList<>(new AnalyseDao().getAll());

        Assert.assertEquals(insertId, allAnalyse.getLast().getId());
    }
}
